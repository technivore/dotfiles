set -o vi
export EDITOR=vim

alias san="svn status | grep '^?' | awk '{print \$2}' | xargs svn add"

# django aliases
alias mp='python manage.py'
alias mpt='python manage.py test'
alias rs='python manage.py runserver_plus --settings=toolbox_site.settings_debug'
alias rsp='python manage.py runserver_plus --settings=toolbox_site.settings_production'
alias rss='python manage.py runserver_plus --settings=toolbox_site.settings_staging'
alias sp='python manage.py shell_plus --settings=toolbox_site.settings_debug'
alias sps='python manage.py shell_plus --settings=toolbox_site.settings_staging'
alias spp='python manage.py shell_plus --settings=toolbox_site.settings_production'
alias dbs='python manage.py dbshell --settings=toolbox_site.settings_debug'
alias destroy-pyc='find . -name \*.pyc -delete'
alias mtt='python manage.py test toolbox --settings=toolbox_site.settings'
alias tp='python manage.py test planner --settings=plantastic.settings_tests'
alias tpc='coverage html --include="apps/*"'
alias deployopenshift='git push openshift HEAD:master'

alias startdocker='docker-machine start default && eval "$(docker-machine env default)"'
alias stopdocker='docker-machine stop default'
alias dockershellinit='eval "$(docker-machine env default)"'
