" don't be strictly vi-compatible
set nocompatible

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vundle setup
filetype off  

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
" Plugin 'marijnh/tern_for_vim'
Plugin 'jelera/vim-javascript-syntax'
Plugin 'pangloss/vim-javascript'
Plugin 'nathanaelkane/vim-indent-guides'
Bundle 'chase/vim-ansible-yaml'
Plugin 'flazz/vim-colorschemes'
Plugin 'luochen1990/rainbow'
Plugin 'ctrlpvim/ctrlp.vim'

call vundle#end()            " required
filetype plugin indent on    " required

" end Vundle setup
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" don't show the preview pane at the top of the window
set completeopt-=preview

" allow modelines
set modeline

" don't unload buffers that are abandoned
set hidden

" define <leader> for mappings
let mapleader = ","
let maplocalleader = ","

" change default encoding to utf-8 (from latin1)
set encoding=utf-8

" turn off BOMs, otherwise the utf-8 encoding would cause them to be prepended
" to new files
set nobomb

" line number
set number

" 2 lines for messages & commands instead of default 1
set cmdheight=2

" display line and column number of current cursor position in the status line
set ruler

" copy indent from current line when starting a new line
set autoindent

" 'smart' autoindenting for new lines. Knows to indent after braces, etc.
set smartindent

" Indenting & Tabs: default tabs to 4 spaces rather than 8, and always expand them into spaces
" number of spaces to use for autoindent
set shiftwidth=4
" number of columns that a tab character in the file counts for
set tabstop=4
" number of columns to insert when hitting <Tab> in insert mode
set softtabstop=4

" always highlight syntax
syntax enable

" enable backspacing through autoindentation, line breaks, and the beginning
" of the current insert. This is the same as `set backspace=2` but more
" explicit.
set backspace=indent,eol,start

" display command as you type it in the status line (in command mode)
set showcmd

" set terminal window title to the name of the current file, if possible
set title

" if the original window title cannot be restored, set it to cwd on exit
let &titleold=getcwd()

" ignore case in searches...
set ignorecase

" ... unless at least one of the characters in the search is upper case
set smartcase

" incremental search highlighting
set incsearch

" highlight searches (use <leader><space> to de-highlight)
set hlsearch

" dont need swap files
set noswapfile

" steve losh's dir setup
set undodir=~/.vim/tmp/undo//     " undo files
set backupdir=~/.vim/tmp/backup// " backups
set directory=~/.vim/tmp/swap//   " swap files

" Make those folders automatically if they don't already exist.
if !isdirectory(expand(&undodir))
    call mkdir(expand(&undodir), "p")
endif
if !isdirectory(expand(&backupdir))
    call mkdir(expand(&backupdir), "p")
endif
if !isdirectory(expand(&directory))
    call mkdir(expand(&directory), "p")
endif

" remember 1000 commands in command history
set history=1000

" always show status line
set laststatus=2

" colors: have to reset completion menu colors manually to get readable menu
colorscheme grb256
hi Pmenu ctermfg=248 ctermbg=0 guifg=#ccccbc guibg=#242424
hi PmenuSel ctermfg=223 ctermbg=235 gui=bold guifg=#ccdc90 guibg=#353a37
hi Comment ctermfg=152

" prevent beeping
set visualbell

" set guifont=Menlo:h14
" set guifont=Inconsolata:h14
set guifont=LiberationMono:h14

" guioptions:
" g: gray out unavailable menu items
" r: right hand scrollbar is always present
" m: menu bar is always present (doesn't do anything in MacVim I don't think)
" e: use GUI-native tabs
set guioptions=grme

" don't wrap lines. Ever.
set nowrap

" back up file while writing but remove backup after write is complete.
set nobackup
set writebackup

" assume a dark terminal background.
set background=dark

" disable mouse in normal mode
set mouse=vi

" why not?
set ttyfast

" pathogen (allows installing vim add-on scripts by just placing them in
" ~/.vim/bundle)
" call pathogen#infect()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" FILETYPE STUFF
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
filetype plugin indent on

au BufRead,BufNewFile *.html,*.htm,*.php,*.tpl setlocal textwidth=0
au BufRead,BufNewFile *.vim,*.txt,*.rst,*.md,*.py setlocal textwidth=79
au BufRead,BufNewFile *.md setlocal filetype=markdown
au BufRead,BufNewFile *.html,*.htm,*.js setlocal sw=2 ts=2 sts=2

" ignore .pyc files when autocompleting filenames
set wildignore=*.pyc

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ABBREVIATIONS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
ab teh the

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" MAPPINGS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" turn off search item highlighting
nnoremap <leader><space> :nohlsearch<cr>:redraw!<cr>

" format xml file
map <F5> <Esc>:1,$!xmllint --format -<CR>

" ,, mapped to 'edit last file'
nnoremap <leader><leader> <c-^>

" = (in visual mode) to run html thru tidy
set equalprg=tidy\ -utf8\ -q\ -i\ --show-body-only\ 1\ --show-warnings\ 0\ --wrap\ 129

" Toggle `set list` (to see tab chars and trailing spaces in file)
nmap <leader>l :set list! list?<CR>

" Use the same symbols as TextMate for tabstops and EOLs
set listchars=tab:▸\ ,eol:¬

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" PYTHON SETTINGS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
au BufRead,BufNewFile *.py set expandtab

" python-specific smartindenting
autocmd BufRead,BufNewFile *.py set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" RAINBOW PARENTHESES
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:rainbow_active = 0

" rainbow_parentheses toggle with ,-r
nnoremap <silent> <Leader>r :RainbowToggle<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" EXPERIMENTAL BITS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Only show cursorline in the current window and in normal mode. (stevelosh)
augroup cline
    au!
    au WinLeave,InsertEnter * set nocursorline
    au WinEnter,InsertLeave * set cursorline
augroup END

" Make sure Vim returns to the same line when you reopen a file. (stevelosh)
augroup line_return
    au!
    au BufReadPost *
        \ if line("'\"") > 0 && line("'\"") <= line("$") |
        \     execute 'normal! g`"zvzz' |
        \ endif
augroup END

" Clean trailing whitespace (stevelosh)
nnoremap <leader>w mz:%s/\s\+$//<cr>:let @/=''<cr>`z

" make patterns 'very magic'; ie perl-compatible
nnoremap / /\v
vnoremap / /\v

" use <tab> instead of % to jump to matching paren/brace/whatever
nnoremap <tab> %
vnoremap <tab> %

" map ; to : because ; is not used anyway so why bother with the shift key.
" Steve Losh is a genius.
nnoremap ; :
